﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjDecorator
{
    //Clase heredada de Bebida
    public class CafeBasico : ComponenteBebida 
    {
        public override double Costo => 1.50; //Parámetro de costo particular establecido
        public override string Descripcion => "Café Básico"; //Descripcion particular de la bebida 
    }
}
