﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EjDecorator
{
    //Clase heredada de Bebida
    public class CafeDescafeinado : ComponenteBebida
    {
        public override double Costo => 2.30; //Parámetro de costo particular establecido
        public override string Descripcion => "Café Descafeinado"; //Descripcion particular de la bebida
    }
}
