﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjDecorator
{
    //Clase heredada de Bebida
    public class CafeMoca : ComponenteBebida
    {
        public override double Costo => 4.10; //Parámetro de costo particular establecido
        public override string Descripcion => "Café Moca"; //Descripcion particular de la bebida
    }
}
