﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjDecorator
{
    //Clase heredada de Bebida
    public class Capuchino : ComponenteBebida
    {
        public override double Costo => 4.25; //Parámetro de costo particular establecido
        public override string Descripcion => "Capuchino"; //Descripcion particular de la bebida
    }
}
