﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjDecorator
{
    //Clase concreta hija de Decorator
    public class Azucar : Decorador
    {
        public Azucar(ComponenteBebida bebida) : base(bebida) { } // Se define el producto al que se le agregará Azúcar
        public override double Costo => _bebida.Costo + 0.25; //Se le agrega el costo individual del condimento al total acumulado de la bebida
        public override string Descripcion => string.Format($"{_bebida.Descripcion}, Azúcar"); //Se agrega la descripción particular del condimento a la del producto

    }
}
