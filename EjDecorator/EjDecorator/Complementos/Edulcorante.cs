﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjDecorator
{
    //Clase concreta hija de Decorator
    public class Edulcorante : Decorador
    {
        public Edulcorante(ComponenteBebida bebida) : base(bebida) { } // Se define el producto al que se le agregará Edulcorante
        public override double Costo => _bebida.Costo + 0.55;  //Se le agrega el costo individual del condimento al total acumulado de la bebida
        public override string Descripcion => string.Format($"{_bebida.Descripcion}, Edulcorante"); //Se agrega la descripción particular del condimento a la del producto
    }
}
