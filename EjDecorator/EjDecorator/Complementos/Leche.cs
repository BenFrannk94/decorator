﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjDecorator
{
    //Clase concreta hija de Decorator
    public class Leche : Decorador
    {
        public Leche(ComponenteBebida bebida) : base(bebida) { } // Se define el producto al que se le agregará Leche
        public override double Costo => _bebida.Costo + 0.75;  //Se le agrega el costo individual del condimento al total acumulado de la bebida
        public override string Descripcion => string.Format($"{_bebida.Descripcion}, Leche"); //Se agrega la descripción particular del condimento a la del producto

    }
}
