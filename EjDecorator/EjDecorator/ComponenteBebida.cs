﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjDecorator
{ // Clase de mayor nivel en la jerarquía, es de tipo abstracta.
    public abstract class ComponenteBebida
    {
        //Operaciones abstractas de solo lectura
        public abstract double Costo { get;} 
        public abstract string Descripcion { get;}

    }
}
