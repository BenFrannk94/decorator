﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjDecorator
{
    //Clase abstracta heredada de CommponenteBebida
    public abstract class Decorador : ComponenteBebida
    {
        //Se define como protected porque sera empleada por los hijos de Decorador (Implementación del Patrón de diseño DECORATOR)
        protected ComponenteBebida _bebida;

        // La instancia de bebida es la agregacion que conecta con ComponenteBebida (Implementación del Patrón de diseño DECORATOR)
        public Decorador (ComponenteBebida bebida)
        {
            _bebida = bebida;
        }
    }
}
