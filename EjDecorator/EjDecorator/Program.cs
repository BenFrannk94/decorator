﻿using System;
using System.Threading.Tasks;

namespace EjDecorator
{
    class Program
    {
        static void Main(string[] args)
        {
            ComponenteBebida cafe = new CafeMoca(); //Se crea una bebida del tipo cafe (Se puede modificar a gusto: CafeBasico,CafeDescafeindado,CafeExpresso,CafeMoca,Capuchino)

            //Se agregan los condimentos a la instancia creada "cafe" (Se pueden agregar al gusto: Azucar,Crema,Edulcorante,Leche)
            cafe = new Crema(cafe);
            cafe = new Azucar(cafe);
            cafe = new Azucar(cafe);
            //Se muestra en pantalla la descripcion del pedido junto con su precio final.
            Console.WriteLine($"Pedido: {cafe.Descripcion} tiene un costo final de: ${cafe.Costo}");
            Console.ReadKey();
        }
    }
}
